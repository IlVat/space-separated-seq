package app

import scala.util.Random

import app.SpaceSeparatedSeq._
import org.scalatest.{FlatSpec, Matchers}

class SpaceSeparatedSeqTest extends FlatSpec with Matchers {
  val dictionary = List("i", "like", "sam", "sung", "samsung", "mobile", "ice",
    "cream", "icecream", "man", "go", "mango")
  val moreThanMaxInput = Random.alphanumeric.take(dictionary.mkString.length + 1).mkString
  val inputWithNumbers = "123samsung"
  val inputWithSpace = "sam sung"
  val emptyLine = ""
  
  "Method compare" should "return true" in {
    compare("samlikesamsung", dictionary) shouldBe true
    compare("samlikesamsung", dictionary) shouldBe true
    compare("manmangogogoicecream", dictionary) shouldBe true
    compare("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", dictionary) shouldBe true
  }
  
  it should "return false" in {
    compare(inputWithNumbers, dictionary) shouldBe false
    compare(inputWithSpace, dictionary) shouldBe false
    compare(emptyLine, dictionary) shouldBe false
    compare(moreThanMaxInput, dictionary) shouldBe false
  }
}
