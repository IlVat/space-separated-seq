package app

object MainApp extends App {
//  val dictionary = io.Source.fromFile("C:\\Projects\\space-separated-seq\\dictionary.txt", "ISO-8859-1").getLines()
//    .toList
  
//  Thread.sleep(5000)
  
  val dictionary = List("i", "like", "sam", "sung", "samsung", "mobile", "ice",
    "cream", "icecream", "man", "go", "mango")
  
  val input = io.StdIn.readLine()
  private val result = if (SpaceSeparatedSeq.compare(input, dictionary)) "Yes" else "No"
  println(result)
}

object SpaceSeparatedSeq {
  
  def compare(input: String, dictionary: List[String]): Boolean = {
    val regex = "^[A-Za-z']$"
    Option(input) match {
      case None => false
      case Some(s) if s.matches(regex) => false
      case Some(s) if s.isEmpty => false
      case Some(s) => isRelevantSeqVer2(s, dictionary)
    }
  }
  
  /**
    * The worth-case complexity of the given algorithm is O(n * n * k)
    * n - input string length
    * k - dictionary size
    *
    **/
  def isRelevantSeqVer1(s: String, dictionary: List[String]): Boolean = {
    var checker = 0
    
    for {
      i <- 0 until s.length if checker == i
      j <- i + 1 to s.length
    } yield {
      if (dictionary.contains(s.slice(i, j))) {
        checker = j
      }
    }
    
    checker == s.length
  }
  
  /**
    * The worth-case complexity of the given algorithm is O(n * k)
    * n - input string length
    * k - dictionary size
    *
    **/
  def isRelevantSeqVer2(s: String, dictionary: List[String]): Boolean = {
    var checker = 0
    
    for {
      i <- 0 until s.length if checker == i
      word <- dictionary
      length = word.length
      end = i + length
      if end <= s.length
      if checker != end
    } {
      if (s.slice(i, end) == word)
        checker = end
    }
  
    checker == s.length
  }
}

